$(function () {

    // Test if canvas is supported. (I'm pointing at you, IE !);
    if (!('getContext' in document.createElement('canvas'))) {
        alert('Sorry, it looks like your browser does not support canvas!');
        return false;
    }

    // The URL of your web server (the port is set in nodejs app);
    var url = 'https://drawmythings.herokuapp.com';
    $('#current-player').hide();

    $('#bestScoreDiv').hide();

    var doc = $(document),
        win = $(window),
        canvas = $('#paper'),
        ctx = canvas[0].getContext('2d'),
        instructions = $('#instructions'),
        input = $('#input'),
        playing = false,
        drawingLine = false;
    inTheGame = false;

    // Generate an unique ID
    var id = Math.round($.now() * Math.random());


    // A flag for drawing activity
    var drawing = false;

    document.getElementById('paper').width = win.width();
    document.getElementById('paper').height = win.height();

    input.css('width', win.width());
    input.css('height', '22px');
    input.prop('disabled', true);

    $(document).keyup(function (evt) {
        if (evt.keyCode == 32) {
            drawingLine = false;
        }
    }).keydown(function (evt) {
        if (evt.keyCode == 32) {
            drawingLine = true;

        }
    });

    $('#Guesses').css('pointer-events', 'none');

    $('#username').keyup(function (e) {
        if (e.keyCode === 13) {
            instructions.fadeOut();
            $('#input').prop('disabled', false);
            socket.emit('enterTheGame', $('#username').val());
        }
    });

    $('#Players').css('pointer-events', 'none');
    $('#bestScoreDiv').css('pointer-events', 'none');

    input.keyup(function (e) {
        if (e.keyCode === 13) {
            if ($('#input').val() === '') {
                return;
            }
            //$('#Guesses').append($('#username').val() + ': ' + $('#input').val() + '<br />');
            var guess = $('#input').val();
            var username = $('#username').val();
            socket.emit('try', {
                guess, username
            });
            $('#input').val('');
        }
    });

    $('#footer').css('height', input.height());

    var clients = {};
    var cursors = {};

    var socket = io.connect(url);
    var inputInterval;


    socket.on('added', function (username) {
        if (username === $('#username').val()) {
            inTheGame = true;
            $('#current-player').show();
        }
    });

    var countdownRunning = false;

    socket.on('currentlyPlaying', function (player) {
        if (player === $('#username').val()) {
            playing = true;
            $('#clearButton').show();
            $('#playing').html('Vous !');
            $('#themeDiv').show();
            input.prop('disabled', true);
            inputInterval = setInterval(function () {
                document.getElementById('input').focus();
            });
        } else {
            playing = false;
            $('#clearButton').hide();
            $('#playing').html(player);
            $('#themeDiv').hide();
            input.prop('disabled', false);
            clearInterval(inputInterval);
        }
    });

    socket.on('bestScore', function (s) {
        if (s.score === 0 || s.username === '') {
            $('#bestScoreDiv').hide();
            return;
        }
        $('#bestScoreDiv').show();
        $('#bestscoreusername').html(s.username);
        $('#bestscore').html(s.score);
    });

    socket.on('countdown', function (count) {
        $('#countdown').html(count);
    });

    socket.on('theme', function (theme) {
        $('#theme').html(theme);
    });

    socket.on('usernames', function (data) {
        $('#Players').html("");
        var usernamesArray = data;
        usernamesArray = usernamesArray.sort(function (a, b) {
            return b[2] - a[2];
        });
        for (var i = 0; i < usernamesArray.length; i++) {
            $('#Players').append('<span style="color: green;">' + usernamesArray[i][2] + '</span>/' + usernamesArray[i][0] + '<br />');
        }
    });

    $('#clearButton').on('mousedown', function (e) {
        socket.emit('clear', 'clearCanvases');
    });

    socket.on('try', function (guess) {
        $('#Guesses').append('<span class="hideMe">' + guess.username + ': ' + guess.guess + '</span><br />');
    });

    socket.on('guessed', function (guess) {
        $('#Guesses').append('<span class="hideMe">' + guess.username + ' a trouvé la réponse. </span><br />');
    });

    socket.on('answer', function (theme) {
        if (theme === '') {
            return;
        }
        $('#Guesses').append('<span class="hideMe">La réponse était : <span style="color: green;">' + theme + '</span>.</span><br />');
    });

    socket.on('allowPlay', function (theme) {
        playing = true;
    });

    socket.on('stopPlay', function () {
        playing = false;
        $('#theme').val('');
    });

    socket.on('clearCanvas', function () {
        document.getElementById('paper').width = document.getElementById('paper').width;
    });

    socket.on('moving', function (data) {

        if (!(data.id in clients)) {
            // a new user has come online. create a cursor for them
            cursors[data.id] = $('<div class="cursor">').appendTo('#cursors');
        }

        // Move the mouse pointer
        cursors[data.id].css({
            'left': data.x,
            'top': data.y
        });

        // Is the user drawing?
        if (data.drawing && clients[data.id]) {

            // Draw a line on the canvas. clients[data.id] holds
            // the previous position of this user's mouse pointer

            var prevX = clients[data.id].x,
                prevY = clients[data.id].y,
                x = data.x,
                y = data.y;

            prevX = Math.floor((win.width() / 1900) * prevX);
            prevY = Math.floor((win.height() / 1000) * prevY);
            x = Math.floor((win.width() / 1900) * x);
            y = Math.floor((win.height() / 1000) * y);

            //drawLine(clients[data.id].x, clients[data.id].y, data.x, data.y);


            drawLine(prevX, prevY, x, y);
        }

        // Saving the current client state
        clients[data.id] = data;
        clients[data.id].updated = $.now();
    });

    var prev = {};

    canvas.on('mousedown', function (e) {
        if (playing) {
            e.preventDefault();
            drawing = true;
            prev.x = e.pageX;
            prev.y = e.pageY;
        }
    });

    doc.bind('mouseup', function (e) {
        if (drawingLine) {
            var x = e.pageX,
                y = e.pageY;
            x = Math.floor((1900 / win.width()) * x);
            y = Math.floor((1000 / win.height()) * y);

            socket.emit('mousemove', {
                'x': x,
                'y': y,
                'drawing': drawing,
                'id': id
            });
            lastEmit = $.now();
        }

        if(!playing){
            return;
        }

        drawLine(prev.x, prev.y, e.pageX, e.pageY);
        drawing = false
    });

    doc.bind('mouseleave', function (e) {
        if (drawingLine) {
            var x = e.pageX,
                y = e.pageY;
            x = Math.floor((1900 / win.width()) * x);
            y = Math.floor((1000 / win.height()) * y);

            socket.emit('mousemove', {
                'x': x,
                'y': y,
                'drawing': drawing,
                'id': id
            });
            lastEmit = $.now();
        }
    });

    var lastEmit = $.now();

    doc.on('mousemove', function (e) {
        if (!playing) {
            return;
        }
        if ($.now() - lastEmit > 20) {
            var x = e.pageX,
                y = e.pageY;
            x = Math.floor((1900 / win.width()) * x);
            y = Math.floor((1000 / win.height()) * y);
            if ((!drawingLine) || (!drawing)) {
                socket.emit('mousemove', {
                    'x': x,
                    'y': y,
                    'drawing': drawing,
                    'id': id
                });
                lastEmit = $.now();
            }

        }

        // Draw a line for the current user's movement, as it is
        // not received in the socket.on('moving') event above

        if (drawing) {
            if (!drawingLine) {
                drawLine(prev.x, prev.y, e.pageX, e.pageY);



                prev.x = e.pageX;
                prev.y = e.pageY;
            }
        }else{
            prev.x = e.pageX;
            prev.y = e.pageY;
        }

    });

    // Remove inactive clients after 10 seconds of inactivity
    setInterval(function () {

        for (ident in clients) {
            if ($.now() - clients[ident].updated > 10000) {

                // Last update was more than 10 seconds ago.
                // This user has probably closed the page

                cursors[ident].remove();
                delete clients[ident];
                delete cursors[ident];
            }
        }

    }, 10000);

    function drawLine(fromx, fromy, tox, toy) {
        ctx.moveTo(fromx, fromy);
        ctx.lineTo(tox, toy);
        ctx.strokeStyle = "#FFFFFF";
        ctx.stroke();
    }

});
